use strict;
use FindBin;
use warnings;
use File::Spec;
use Test::More tests => 5;

require_ok('Geoffrey::Changelog::Yaml');
use_ok 'Geoffrey::Changelog::Yaml';

my $object = Geoffrey::Changelog::Yaml->new();

my $path = File::Spec->catfile( $FindBin::Bin, 'data', 'changelog', 'changelog' );
my $main = $object->load($path);

#File::Spec->catfile( $folder, "changelog-$_" ) ;

foreach ( @{ $main->{changelogs} } ) {
    my $file = File::Spec->catfile( $FindBin::Bin, 'data', 'changelog', 'changelog-' . $_ );
    foreach ( @{ $object->load($file) } ) {

    }
}

is(
    $object->tpl_main, q~
---
templates:
    - name: tpl_std
      columns:
      - name: id
        type: integer
        notnull: 1
        primarykey: 1
        default: autoincrement

prefix: smpl
postfix: end

changelogs: 
  - "01"
~, 'changelog_table sub test'
);

is(
    $object->tpl_sub, q~- id: 001.01-maz
  author: "Mario Zieschang"
  entries:
    - action: table.add
      name: 'client'
      template: 'tpl_std'
~, 'changelog_type sub test'
);

is(
    $object->write( q~.~, { test => { foo => 'bar' } }, 1 ), q~---
test:
  foo: bar
~, 'changelog_type sub test'
);
