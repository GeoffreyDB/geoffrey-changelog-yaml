
use strict;
use FindBin;
use warnings;
use File::Spec;
use Test::Exception;
use Test::More tests => 12;

require_ok('DBI');
use_ok 'DBI';

require_ok('Geoffrey');
use_ok 'Geoffrey';

require_ok('Geoffrey::Converter::SQLite');
use_ok 'Geoffrey::Converter::SQLite';

my $converter = Geoffrey::Converter::SQLite->new();
dies_ok { $converter->check_version('3.0') } 'underneath min version expecting to die';
is( $converter->check_version('3.7'), 1, 'min version check' );
is( $converter->check_version('3.9'), 1, 'min version check' );

my $dbh = DBI->connect("dbi:SQLite:database=.tmp.sqlite");
my $object = new_ok( 'Geoffrey' => [ dbh => $dbh ] ) or plan skip_all => "";
ok $object->read( File::Spec->catfile( $FindBin::Bin, 'data', 'changelog' ) ), 'read everything to database';

throws_ok { $converter->index->drop() } 'Geoffrey::Exception::RequiredValue::IndexName', 'Drop index needs a name';

$object->disconnect();
